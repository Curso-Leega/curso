## Compute Engine

**Máquinas virtuais escalonáveis e de alto desempenho.**

O Compute Engine oferece máquinas virtuais configuráveis em execução nos data centers do Google, com acesso a infraestruturas de redes de alto desempenho e armazenamento em blocos.

1 - Escolha a VM certa para suas necessidades, seja para tarefas gerais seja para carga de trabalho otimizada, em tamanhos de máquina predefinidos ou personalizados.

2 - Integre a computação a outros serviços do Google Cloud, como IA/ML e análise de dados.

3 - Faça reservas para garantir que seus aplicativos tenham a capacidade necessária para o escalonamento. Economize com os descontos por uso prolongado ou por uso contínuo ao executar cargas de trabalho de computação.

**Principais recursos**

**Migração em tempo real para VMs**

As máquinas virtuais do Compute Engine podem migrar em tempo real entre sistemas host sem necessidade de reinicialização, o que mantém seus aplicativos em execução mesmo durante manutenções.

**VMs preemptivas**

Realize jobs em lote e cargas de trabalho tolerantes a falhas em VMs preemptivas para reduzir seus custos de vCPU e memória em até 80%, mantendo o mesmo desempenho e os recursos das VMs comuns.

**Preço**

Os preços do Compute Engine são baseados no uso por segundo dos tipos de máquina, discos permanentes e outros recursos que você escolher para suas máquinas virtuais. Faça uma avaliação gratuita do Compute Engine.

https://cloud.google.com/compute/all-pricing

---

## Cloud Functions

**Conceitos**

O Cloud Functions é a plataforma de computação sem servidor com base em eventos do Google Cloud. Execute seu código no local ou na nuvem sem a necessidade de provisionar servidores. Avance da etapa de código até implantação com entrega contínua e ferramentas de monitoramento. Aumente ou reduza a escala do Cloud Functions para pagar somente pelos recursos de computação utilizados. Conecte-se ao serviços do Google Cloud ou de terceiros atuais para criar cenários de desenvolvimento complexos com facilidade.
•	Dispensa provisionamento, gerenciamento ou upgrade.
Ajuste a escala de acordo com a carga
Simplifique o desenvolvimento de aplicativos complexos em diferentes linguagens
Monitoramento, registro e rastreamento distribuído, tudo integrado
Segurança integrada no nível do papel e por função com base no princípio do menor privilégio
Recursos de rede importantes para cenários híbridos e de várias nuvens

**Preço**

O preço do Cloud Functions é determinado de acordo com o ambiente de execução da função, quantas vezes ela é invocada e quantos recursos são provisionados.

https://cloud.google.com/functions

**FUNÇÃO PRINCIPAL**

import storage

import json

import pandas as pd

from google.cloud import storage

import unidecode

def hello_gcs(event, context):

    file = event
	
    print(f"Processing file: {file['name']}.")
	
    print(file['name'])
	
    print(file)
	
    storage_client = storage.Client()
	
    bucket = storage_client.get_bucket(file['bucket'])
	
    blob = bucket.blob(file['name'])
	
    json_data = blob.download_as_string()
	
    json_str = json_data.decode('utf8').replace('\\n', '').replace('\\c','').replace('\\r','')
	
    data = json.dumps(json_str)
	
    print(data)
	
**REQUIREMENTS**	

storage>=0.0.4.3

google-cloud-storage>=1.26.0

pandas>=1.0.3

unidecode>=1.1.1

---

## Cloud Data Lab

**CONCEITOS**

•	Exploração de dados avançada:
O Cloud Datalab é uma poderosa ferramenta interativa, criada para explorar, analisar, transformar, visualizar dados e criar modelos de machine learning no Google Cloud Platform. Ele é executado no Compute Engine.

•	Código aberto e integrado:
O Cloud Datalab é desenvolvido no Jupyter (anteriormente conhecido como IPython)
O Cloud Datalab permite a análise dos seus dados nas ferramentas BigQuery, Cloud Machine Learning Engine, Compute Engine e Cloud Storage usando Python, SQL e JavaScript.

•	Preços do Cloud Datalab:
O uso do Google Cloud Datalab não é cobrado. No entanto, você paga por todos os recursos do Google Cloud Platform usados com o Cloud Datalab, como:
Recursos de computação: haverá cobranças desde o momento da criação até a exclusão da instância da VM do Cloud Datalab.
Serviços de análise dos dados: são cobrados custos com o Google BigQuery na emissão de consultas SQL dentro dos notebooks do Cloud Datalab.
Para minimizar os custos, você pode interromper as instâncias de VM do Cloud Compute Engine quando elas não estiverem em uso.

**Início rápido**


Crie uma instância do Cloud Datalab. O nome da instância precisa começar com uma letra minúscula, seguida de até 62 letras minúsculas, números ou hifens e não pode terminar com um hífen.

**criando um DataLab**

datalab create datalab-leega-(seu nome)

**Parando um DataLab**

datalab stop (instância)

**Conectando em um DataLab**

datalab connect (instância)

**Deletando um DataLab**

Haverá cobranças desde o momento da criação até a exclusão da instância da VM do Cloud Datalab (consulte Preços do Cloud Datalab). Também haverá cobranças pelo disco permanente onde os blocos de notas são armazenados. O disco permanece após a exclusão da VM até que você o exclua. O comando a seguir exclui a instância da VM e o disco permanente.

datalab delete --delete-disk instance-name

---

## Carregando um arquivo do Cloud Storage para o BigQuery


**Instalação das bibliotecas necessárias**

!!pip install google-cloud

!!pip install storage

!!pip install google_bigquery

!!pip install --upgrade shell

**Importando as  bibliotecas necessárias**

from google.cloud import bigquery

import storage

import json

import pandas_gbq

import pandas as pd

import numpy as np

import pandas_gbq

from datetime import date

import os

from shell import shell


**Executando o JOB de Carga**

client = bigquery.Client()

table_ref = client.dataset('leega_demo_datalab').table('airline_id_codes')

job_config = bigquery.LoadJobConfig()

job_config.write_disposition = bigquery.WriteDisposition.WRITE_TRUNCATE

job_config.source_format = bigquery.SourceFormat.NEWLINE_DELIMITED_JSON

job_config.autodetect = True

uri = "gs://curso-bigquery-leega/airline_id_codes.json"

load_job = client.load_table_from_uri(
    uri, table_ref, job_config=job_config
) 

print("Iniciando o job {}".format(load_job.job_id))

load_job.result()  

print("Job Finalizado.")

destination_table = client.get_table(table_ref)

print("{} Linhas Carregadas.".format(destination_table.num_rows))

## Trabalhando com Storage

**Baixando Arquivos do Storage**

os.system("gsutil cp gs://curso-bigquery-leega/airline_id_codes.json airline_id_codes.json")

**modificações necessárias para um arquivo baixado**

with open('airline_id_codes2.json') as file_data:
    data = file_data.readlines()
	
for i, item in enumerate(data):
    data[i] = json.loads(item.replace('[\'[\'', '[\'').replace('\']\']', '\']'),encoding=("utf-8"))	

**Gravando arquivos no Storage**

os.system("gsutil cp airline_id_codes2.json gs://curso-bigquery-leega")

**Listando arquivos do Storage**

**referência**

https://cloud.google.com/storage/docs/gsutil/commands/cp?hl=de



## Trabalhando com o BigQuery

**Usando a biblioteca Shell**

dados = shell("""bq query --use_legacy_sql=false \
    'SELECT
       *
     FROM
       `leega_demo_datalab`.airline_id_codes' LIMIT 10""")
	   
for dados in dados.output():

     print (dados)	   
	   
**Usando o método Client() da biblioteca google_bigquery**

client = bigquery.Client()

sql = """
    SELECT * FROM leega_demo_datalab.airline_id_codes LIMIT 10
""" 

result = client.query(sql).to_dataframe()

print(result)
	   
## Abrindo e modificando um arquivo com Pandas

**Abrindo um Arquivo local**

with open('airline_id_codes.json') as file_data:
    data = file_data.readlines()
    
for i, item in enumerate(data):
    data[i] = json.loads(item) 
	
**verificando se o arquivo foia berto com sucesso**

print(data)

**Criando nosso DataFrame**

df = pd.DataFrame(data)

**Manipulando o Data Frame**

df['code'] = df['code'].astype(np.float64)


**Gravando na Tabela**

pandas_gbq.to_gbq(df, 'leega_demo_datalab' + '.' + 'airline_id_codes', project_id='leega-demo-manserv', if_exists='replace')

---

## Comandos Úteis

**Quantidade de linhas e colunas do DataFrame: **
df.shape

**Descrição do Index: **
df.index

**Colunas presentes no DataFrame: **
df.columns

**Contagem de dados não-nulos: **
df.count()

**Removendo linhas pelo index: **
df.drop([0, 1])

**Removendo colunas utilizando o argumento axis=1: **
df.drop('code', axis=1)

**Criando uma nova coluna em um DataFrame: **
df['Nova_Coluna'] = 0

**Data Atual: **
date.today()

**Renomeando colunas de um DataFrame. Se seu DataFrame possui 3 colunas, passe 3 novos valores em uma lista: **

df.columns = ['Coluna 1', 'Coluna 2', 'Coluna 3']

**Soma dos valores de um DataFrame: **
df.sum()

**Menor valor de um DataFrame: **
df.min()

**Maior valor: **
df.max()

**Index do menor valor: **
df.idmin()

**Index do maior valor: **
df.idmax()

**Resumo estatístico do DataFrame, com quartis, mediana, etc.: **
df.describe()

**Média dos valores: **
df.mean()

**Mediana dos valores: **
df.median()

**Aplicando uma função que substitui a por b: **
df.apply(lambda x: x.replace('a', 'b'))

**Ordenando valores em ordem crescente: **
df.sort_values()

**Ordenando valores em ordem decrescente: ** 
df.sort_values(ascending=False)



**referência**

https://pypi.org/

https://www.caelum.com.br/apostila-python-orientacao-objetos/



**Exercícios**


1 - Baixe o arquivo airline_id_codes.json do storage adicionando seu nome no final do mesmo (airline_id_codes_(NOME).json) e mostre as 10 primeiras linhas do arquivo

2 - Criar um DataFrame com o arquivo .json baixado exclua a coluna "code" mostre as 10 primeiras linhas e as colunas do DataFrame.

3 - Criar um DataFrame com o arquivo .json baixado Adicione uma coluna com a Data Atual e carregue o arquivo em uma tabela no BigQuery.

4 - Crie uma Função que receba como parâmetro Dataset e Nome da tabela, ela deve carregar o arquivo baixado no BigQuery.

